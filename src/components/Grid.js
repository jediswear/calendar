import { FORMAT } from "./Calendar";
import moment from "moment";
import DOMNode from "src/components/DOMNode";

const GRID_CELLS_COUNT = 35

class Grid {
  constructor (root) {
    this.root = root;
    this.dataDates = [];
    this.dateNodes = this.#createDateNodes();
  }

  destroy(){
    this.root.innerHTML = ""
  }

  #createDateNodes (){
    return new Array(GRID_CELLS_COUNT).fill(true).map(() => {
      const node = new DOMNode("div")
      node.addClass("date")

      return node
    })
  }

  #mountCells(){
    this.dateNodes.forEach(node => this.root.appendChild(node.element))
  }

  render (currentMonth) {
    this.#mountCells()
    const prevMonth = moment(currentMonth).subtract(1, "month");
    const nextMonth = moment(currentMonth).add(1, "month");
    const currentStartIndex = moment(currentMonth).date(1).format("E") - 1;

    const prevDays = getArrayOfDates(prevMonth);
    const currentDays = getArrayOfDates(currentMonth);
    const nextDays = getArrayOfDates(nextMonth);

    prevDays.splice(0, prevDays.length - currentStartIndex);
    nextDays.splice(35 - (prevDays.length + currentDays.length));

    this.dataDates = [...prevDays, ...currentDays, ...nextDays];

    this.dateNodes.forEach((node, i) => {
      if (i < prevDays.length || i >= prevDays.length + currentDays.length) { node.addClass("other-month"); } else node.removeClass("other-month");

      node.element.dataset.date = this.dataDates[i];
      node.element.innerHTML = `<span class="date-label">${moment(
        this.dataDates[i],
        FORMAT
      ).format("D")}</span>`;
    });
  }
}

export default Grid;

function getArrayOfDates (month) {
  const total = month.daysInMonth();
  const days = [];

  for (let i = 1; i <= total; i++) days.push(month.date(i).format(FORMAT));

  return days;
}
