class DOMNode {
  constructor (type) {
    this.element = document.createElement(type);
  }

  addClass (name) {
    this.element.classList.add(name);
    return this;
  }

  removeClass (name) {
    this.element.classList.remove(name);
    return this;
  }

  style (name, value) {
    this.element.style[name] = value;
    return this;
  }
}

export default DOMNode;
