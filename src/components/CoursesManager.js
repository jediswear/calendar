import { difference, hasElem, unique } from "src/components/Calendar";

class CoursesManager {
  constructor(courses) {
    this.courses = courses;
  }

  manage() {
    /* group cross education courses */
    const groups = this.groupCrossCourses();
    /* divide cross courses on types: overlay and contains */
    const combinedGroups = this.combine(groups);

    this.updateCourses(combinedGroups);
  }

  combine(crossGroups) {
    return crossGroups.map((group) => this.combineGroup(group));
  }

  updateCourses(groups) {
    this.courses = groups.flat().flat();
  }

  combineGroup(group) {
    const res = group.reduce((acc, course) => {
      if (group.length === 1) {
        return [...acc, [course]];
      }

      const block = [];
      const filteredGroup = unique(group, course).filter(
        (el) => !hasElem(acc, el)
      );
      const coveredElem = filteredGroup.find(
        (elem) =>
          this.crossType(elem, course) === "overlay" ||
          this.crossType(elem, course) === "contains"
      );

      const hasCovered = hasElem(acc, coveredElem);
      const hasCourse = hasElem(acc, course);

      !hasCourse && block.push(course);
      coveredElem && !hasCovered && block.push(coveredElem);

      if (block.length >= 1) {
        return [...acc, block];
      } else {
        return acc;
      }
    }, []);

    res.forEach((items) => {
      this.setPosX(items);
      this.setPosY(items);
      this.setCoverType(group);
      this.setCoverBy(items);
      this.calcMinLength(items);
    });

    return res;
  }

  crossType(one, another) {
    const { start: firstStart, end: firstEnd } = one;
    const { start: secondStart, end: secondEnd } = another;

    /**
     * check course overlay or contain
     * */
    const res =
      (difference(secondStart, firstStart) > 0 &&
        difference(secondEnd, firstEnd) > 0) ||
      (difference(secondStart, firstStart) < 0 &&
        difference(secondEnd, firstEnd) < 0)
        ? "overlay"
        : "contains";

    return res;
  }

  calcMinLength(courses) {
    if (courses.length < 2) {
      courses.forEach((el) => (el.currLength = el.fullLength));
      return;
    }

    courses.forEach((course, i, arr) => {
      const next = arr[i + 1];

      if (!next) return;

      course.minLength = difference(course.start, next.start);

      next.minLength = next.overlay ? difference(course.end, next.end) : 0;
      next.currLength = next.overlay ? difference(course.end, next.end) : 0;
    });
  }

  setCoverBy(group) {
    group.forEach((course, i, arr) => {
      const cover = i === 0 ? arr[1] : arr[0];

      // TODO: remove id
      cover && (course.coverBy = cover.id);
      cover && (course.crossedCourse = cover);
    });
  }

  setPosX(group) {
    group.forEach((course, i) => (course.posX = i === 0 ? "start" : "end"));
  }

  setPosY(group) {
    switch (group.length) {
      case 2:
        group.forEach((course) => (course.posY = "first"));
        break;
      case 1:
        group.forEach((course) =>
          course.coverBy ? (course.posY = "first") : (course.posY = "second")
        );
        break;
    }
  }

  setCoverType(group) {
    if (group.length === 1) return;

    const first = group[0];
    const second = group[1];

    const type = this.crossType(first, second);

    group.forEach((course) => (course[type] = true));
  }

  groupCrossCourses() {
    const res = this.courses.map((firstCourse) => {
      return this.courses
        .filter((secondCourse) => this.isCrossed(firstCourse, secondCourse))
        .sort((el1, el2) => difference(el2.start, el1.start));
    });

    return this.removeDuplicates(res);
  }

  isCrossed(one, another) {
    const { start: firstStart, end: firstEnd } = one;
    const { start: secondStart, end: secondEnd } = another;

    return (
      difference(firstStart, secondEnd) >= 0 &&
      difference(firstEnd, secondStart) <= 0
    );
  }

  iterateCourses(cb) {
    this.courses.forEach((course) => cb(course));
  }

  removeDuplicates(arr) {
    const result = arr.reduce((acc, group) => {
      const ids = this.collectIds(group).toString();
      const isUniq = acc.find(
        (group) => this.collectIds(group).toString() === ids
      );

      if (!isUniq) {
        return [...acc, group];
      } else {
        return acc;
      }
    }, []);

    return result;
  }

  collectIds(arr) {
    return [...new Set(arr.map((item) => item.id))];
  }
}

export default CoursesManager;
