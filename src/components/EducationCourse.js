import {
  datePosInWeek,
  DAYS_IN_WEEK,
  difference,
  FORMAT,
  mom,
} from "src/components/Calendar";
import Chunk from "src/components/Chunk";
import { colors } from "src/scripts";
import Interval from "src/components/Interval";
import BigDot from "src/components/BigDot";

class EducationCourse {
  #interval;
  #widthOfOnePercent;

  constructor({ type, id, name, interval }) {
    this.name = name;
    this.type = type;
    this.id = id;
    this.chunks = [];
    this.posX = null;
    this.posY = null;
    this.crossedCourse = null;
    this.crossedInterval = null;
    this.#widthOfOnePercent = 0;
    this.minLength = 0;
    this.currLength = 0;
    this.#interval = interval;
    this.bigDots = [];
    this.isCollapsed = false;
  }

  get start() {
    return this.#interval.start;
  }

  get end() {
    return this.#interval.end;
  }

  get duration() {
    return this.#interval.duration;
  }

  iterateChunks(cb) {
    this.chunks.forEach((chunk) => cb(chunk, this));
  }

  prepare(widthOfOnePercent) {
    this.#widthOfOnePercent = widthOfOnePercent;

    // 1. split to chunks
    this.#split();

    // 2. calc crossed interval
    this.#createCrossButtons();

    // 3. initial toggle
    this.#toggle();

    // 4. add toggle listener
    this.#addToggleListener();
  }

  #createCrossButtons() {
    this.#calcCrossedInterval();

    if (this.crossedInterval) {
      this.crossedInterval.forEachDate((date) => {
        // TODO: move this.color from chunk to period ??
        const bigDot = new BigDot(date);
        bigDot.color = colors[this.type];

        bigDot.onClick(() => {
          this.bigDots.forEach((bigDot) => {
            bigDot.color =
              colors[!this.isCollapsed ? this.type : this.crossedCourse.type];
          });

          this.#toggle();

          if (this.crossedCourse) {
            this.crossedCourse.#toggle();
          }
        });
        this.bigDots.push(bigDot);
      });
    }
  }

  static calcChunksAmount(start, end) {
    const momentStart = mom(start).startOf("isoWeek");
    const momentEnd = mom(end).endOf("isoWeek");
    const daysInRow = difference(momentStart, momentEnd);
    const chunksAmount = (daysInRow + 1) / DAYS_IN_WEEK;

    return chunksAmount;
  }

  #createIntervals(start, end) {
    const intervals = [];
    const chunksAmount = EducationCourse.calcChunksAmount(start, end);

    if (chunksAmount === 1) {
      intervals.push(new Interval(start, end));
    } else {
      for (let iteration = 0; iteration < chunksAmount; iteration++) {
        let startDate;
        let endDate;

        if (iteration === 0) {
          startDate = start;
          endDate = datePosInWeek("endOf", start).format(FORMAT);
        } else if (iteration === chunksAmount - 1) {
          startDate = datePosInWeek("startOf", end).format(FORMAT);
          endDate = end;
        } else {
          startDate = datePosInWeek("startOf", start)
            .add(iteration, "weeks")
            .format(FORMAT);
          endDate = datePosInWeek("endOf", start)
            .add(iteration, "weeks")
            .format(FORMAT);
        }

        intervals.push(new Interval(startDate, endDate));
      }
    }

    return intervals;
  }

  #split() {
    this.chunks = this.#createIntervals(this.start, this.end).map(
      (interval) => {
        const chunk = new Chunk({
          name: this.name,
          interval: interval,
        });
        chunk.color = colors[this.type];
        chunk.createDOMItem(this.id, this.posX, this.posY);

        return chunk;
      }
    );
  }

  #recomputeChunksWidth(length) {
    const reducedEnd = mom(this.start)
      .add(length - 1, "days")
      .format(FORMAT);
    const end = length ? reducedEnd : this.end;

    this.iterateChunks((chunk) => chunk.resetWidth());

    const intervals = this.#createIntervals(this.start, end);

    this.#updateChunksWidth(intervals);
  }

  #updateChunksWidth(intervals) {
    intervals.forEach((interval, i) => {
      if (interval === null) {
        this.chunks[i].resetWidth();
        return;
      }

      const targetChunk = this.chunks[i];
      targetChunk.width = interval.duration;
    });
  }

  #applyWidthToDOMItem() {
    this.chunks.forEach(({ node, width }) => {
      const percent = (100 / DAYS_IN_WEEK) * width;
      const pxWidth = percent * this.#widthOfOnePercent - 1;

      node.style("width", `${pxWidth >= 0 ? pxWidth : 0}px`);
    });
  }

  #toggleCollapse() {
    const length =
      this.currLength === this.minLength ? this.duration : this.minLength;

    if (this.posX === "start") {
      this.#recomputeChunksWidth(length);
    } else {
      this.#recomputeChunksWidthReverse(length);
    }

    this.currLength = length;
  }

  #recomputeChunksWidthReverse(length) {
    const reducedStart = mom(this.end)
      .subtract(length - 1, "days")
      .format(FORMAT);
    const start = length ? reducedStart : this.end;

    this.chunks.forEach((chunk) => chunk.resetWidth());

    const newInterval = this.#createIntervals(
      length === 0 ? mom(start).add(1, "days") : start,
      this.end
    );

    const difference = this.chunks.length - newInterval.length;

    // refactor this
    for (let i = 0; i < difference; i++) {
      newInterval.unshift(null);
    }

    this.#updateChunksWidth(newInterval);
  }

  #toggleNavDots() {
    if (this.posX === "start") {
      const chunkNode = this.chunks[0].node;

      if (this.minLength === this.currLength) {
        chunkNode.addClass("showDots");
      } else {
        chunkNode.removeClass("showDots");
      }
    } else {
      const chunkNode = this.chunks[this.chunks.length - 1].node;

      if (this.minLength === this.currLength) {
        chunkNode.addClass("showDots");
      } else {
        chunkNode.removeClass("showDots");
      }
    }
  }

  #toggle() {
    this.isCollapsed = !this.isCollapsed;

    this.#toggleCollapse();
    this.#toggleNavDots();
    this.#applyWidthToDOMItem();
  }

  #addToggleListener() {
    this.iterateChunks((chunk) => {
      chunk.dotsOnClick(() => {
        this.#toggle();

        if (this.crossedCourse) {
          this.crossedCourse.#toggle();
        }
      });
    });
  }

  #calcCrossedInterval() {
    if (this.crossedCourse && this.posX === "start") {
      this.crossedInterval = new Interval(this.crossedCourse.start, this.end);
    }
  }
}

export default EducationCourse;
