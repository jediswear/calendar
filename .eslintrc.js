module.exports = {
  env: {
    browser: true,
    es2020: true
  },
  extends: ["semistandard"],
  rules: {
    quotes: [2, "double", { avoidEscape: true, allowTemplateLiterals: true }]
  },
  parserOptions: {
    ecmaVersion: 11,
    sourceType: "module"
  },
  parser: "babel-eslint"
};
