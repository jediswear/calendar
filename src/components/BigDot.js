import DOMNode from "src/components/DOMNode";

class BigDot {
  constructor(date) {
    // TODO: maybe remove
    this.date = date;
    this.node = this.#createDot();
  }

  set color (color) {
    this.node.style("background", color)
  }

  onClick(cb){
    this.node.element.addEventListener("click", cb)
  }

  #createDot() {
    const node = new DOMNode("span");
    node.addClass("big-dot");
    node.addClass("active");

    return node
  }
}

export default BigDot;
