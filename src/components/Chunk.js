import DOMNode from "src/components/DOMNode";

class Chunk {
  #interval

  constructor ({ name, width = 0, interval }) {
    this.node = null;
    this.color = "#000000";
    this.name = name;
    this.width = width;
    this.DOMDots = null;
    this.#interval = interval
  }

  get start (){
    return this.#interval.start
  }

  get end (){
    return this.#interval.end
  }

  get duration (){
    return this.#interval.duration
  }

  createDOMItem (id, posX, posY) {
    const node = new DOMNode("div");
    this.DOMDots = posY === "first" ? this.createDOMDots() : null;
    const name = this.createDOMName();

    node.element.dataset.id = id;
    node.addClass(posX);

    if (posX === "start") {
      node.element.appendChild(name);
      this.DOMDots && node.element.appendChild(this.DOMDots);
    } else {
      this.DOMDots && node.element.appendChild(this.DOMDots);
      node.element.appendChild(name);
    }

    this.node = node;
  }

  createDOMName () {
    const startLabel = new DOMNode("span");
    startLabel.addClass("begin-mark");

    const nameItem = new DOMNode("span");
    nameItem
      .addClass("name")
      .style("background", this.color)
      .element.appendChild(startLabel.element);

    nameItem.element.append(this.name);

    return nameItem.element;
  }

  createDOMDots () {
    const dotsItem = new DOMNode("span");
    dotsItem.addClass("dots");

    for (let i = 0; i <= 2; i++) {
      const dot = new DOMNode("span");
      dot.addClass("dot").style("background", this.color);
      dotsItem.element.appendChild(dot.element);
    }

    return dotsItem.element;
  }

  dotsOnClick (cb) {
    this.DOMDots && this.DOMDots.addEventListener("click", cb);
  }

  resetWidth () {
    this.width = 0;
  }
}

export default Chunk;
