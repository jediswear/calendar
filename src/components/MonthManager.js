import moment from "moment";

class MonthManager {
  constructor(initialMonth) {
    this.current = initialMonth || moment().subtract(14, "month");
    this.monthLabel = null;
    this.yearLabel = null;
    this.onChange = null;
  }

  next = () => {
    this.current.add(1, "month");
    this.updateLabels();

    if (typeof this.onChange === "function") {
      this.onChange();
    }
  };
  prev = () => {
    this.current.subtract(1, "month");
    this.updateLabels();

    if (typeof this.onChange === "function") {
      this.onChange(this.current);
    }
  };

  addListeners(prev, next) {
    prev.addEventListener("click", this.prev);
    next.addEventListener("click", this.next);
  }

  updateLabels() {
    this.monthLabel.innerHTML = this.current.locale("ru").format("MMMM");
    this.yearLabel.innerHTML = this.current.format("YYYY");
  }

  setMontLabel(label) {
    this.monthLabel = label;
  }

  setYearLabel(label) {
    this.yearLabel = label;
  }
}

export default MonthManager;
