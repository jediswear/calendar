import moment from "moment";
import CoursesManager from "src/components/CoursesManager";
import Grid from "src/components/Grid";
import EducationCourse from "src/components/EducationCourse";
import Interval from "src/components/Interval";
import MonthManager from "src/components/MonthManager";

const initialMonth = moment().subtract(14, "month");

class NewCalendar {
  constructor(calendarNode, courses) {
    this.calendarNode = calendarNode;
    this.courses = courses;
    this.width = this.calendarNode.offsetWidth;
    this.widthOfOnePercent = this.width / 100;
    this.grid = new Grid(this.calendarNode.querySelector(".dates"));
    this.monthManager = new MonthManager(initialMonth);
  }

  build() {
    // TODO: implement responsive
    // const desktop = window.innerWidth >= 768;

    const prev = this.calendarNode.querySelector(".prev");
    const next = this.calendarNode.querySelector(".next");
    const montLabel = this.calendarNode.querySelector(
      ".calendar-head .content .title"
    );
    const yearLabel = this.calendarNode.querySelector(
      ".calendar-head .content .subtitle"
    );
    this.monthManager.addListeners(prev, next);
    this.monthManager.setMontLabel(montLabel);
    this.monthManager.setYearLabel(yearLabel);
    this.monthManager.updateLabels();
    this.monthManager.onChange = (month) => {
      this.grid.destroy()
      this.render(month);
    }

    this.render(this.monthManager.current);
  }

  render(month) {
    this.grid.render(month);

    const educationCourses = this.courses.map((course) => {
      const interval = new Interval(course.start, course.end);
      return new EducationCourse({
        type: course.type,
        id: course.id,
        name: course.name,
        interval,
      });
    });

    const coursesManager = new CoursesManager(educationCourses);
    coursesManager.manage();
    coursesManager.iterateCourses(this.setupCourse);
  }

  setupCourse = (course) => {
    course.prepare(this.widthOfOnePercent);
    // paste chunks on grid
    course.iterateChunks(this.pasteToGrid);

    // TODO: refactor
    course.bigDots.forEach((bigDot) => {
      const targetNode = this.grid.dateNodes.find(
        (node) => node.element.dataset.date === bigDot.date
      );

      if (targetNode) {
        targetNode.element.appendChild(bigDot.node.element);
      }
    });
  };

  pasteToGrid = (chunk, course) => {
    const { posY, posX } = course;

    this.grid.dateNodes.forEach((dateNode) => {
      const { date } = dateNode.element.dataset;

      const placeForStart = posX === "start" && chunk.start === date;
      const placeForEnd = posX === "end" && chunk.end === date;

      if (placeForStart || placeForEnd) {
        posY !== "first" && chunk.node.addClass("secondLine");
        dateNode.element.appendChild(chunk.node.element);
      }
    });
  };
}

export default NewCalendar;
