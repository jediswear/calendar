import { FORMAT } from "src/components/Calendar";
import moment from "moment";

class Interval {
  #startDate;
  #endDate;

  constructor(startDate, endDate) {
    this.#startDate = moment(startDate, FORMAT);
    this.#endDate = moment(endDate, FORMAT);
  }

  get duration() {
    return Math.abs(this.#calcDuration());
  }

  get end() {
    return this.#endDate.format(FORMAT);
  }

  get start() {
    return this.#startDate.format(FORMAT);
  }

  forEachDate(cb) {
    new Array(this.duration).fill(true).forEach((item, index) => {
      const date = moment(this.#startDate).add(index, "days");
      cb(date.format(FORMAT));
    });
  }

  #calcDuration() {
    return this.#endDate.diff(this.#startDate, "days") + 1;
  }
}

export default Interval;
