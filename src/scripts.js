import "./styles/styles.scss";
import NewCalendar from "src/components/NewCalendar";
import Calendar from "src/components/Calendar";

const calendarItem = document.querySelector('.calendar'),
      calendar     = new Calendar(calendarItem)

// calendar.buildCalendar()

export const colors = {
  course: "#ED5454",
  masterClass: "#4BBCC1",
  training: "#9B2997",
  seminar: "#346FF1",
  webinar: "#FD9B5B"
}

const courses = [
  {
    id: "1",
    name: "Моделирование процессов и систем",
    start: "01.05.2019",
    end: "07.05.2019",
    type: "webinar"
  },
  {
    id: "2",
    name: "Риск-менеджмент",
    start: "03.05.2019",
    end: "08.05.2019",
    type: "course"
  },
  {
    id: "3",
    name: "Основы права",
    start: "07.05.2019",
    end: "13.05.2019",
    type: "training"
  },
  {
    id: "4",
    name: "Компакт курс з МСФ3",
    start: "15.05.2019",
    end: "19.05.2019",
    type: "seminar"
  },
  {
    id: "5",
    name: "Математическая физика",
    start: "21.05.2019",
    end: "26.06.2019",
    type: "course"
  },
  {
    id: "6",
    name: "Аккумуляторы, топливные элементы и их роль в современном мире",
    start: "24.05.2019",
    end: "03.06.2019",
    type: "masterClass"
  },
  {
    id: "7",
    name: "Экономика минерального сырья",
    start: "22.05.2019",
    end: "12.06.2019",
    type: "training"
  },
  {
    id: "8",
    name: "Суперкомпьютеры и параллельная обработка данных",
    start: "14.05.2019",
    end: "16.05.2019",
    type: "masterClass"
  }
];

const newCalendar = new NewCalendar(calendarItem, courses)
newCalendar.build()